# AT Tools

AT Tools provides theme and admin features that may be required by your theme.
This is now a required module for Adaptivetheme in Drupal

For a full description of the module, visit the
[project page](https://www.drupal.org/project/at_tools).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/at_tools).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [AT Theme Generator](https://www.drupal.org/project/AT_theme_generator)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Ivan Trokhanenko - [i-trokhanenko](https://www.drupal.org/u/i-trokhanenko)
- Jeff Burnz - [Jeff Burnz](https://www.drupal.org/u/jeff-burnz)
